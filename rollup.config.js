import vue from 'rollup-plugin-vue';
import typescript from 'rollup-plugin-typescript';


export default {
  input: 'src/client/app.ts',
  output: {
    file: 'dist/static/js/bundle.js',
    format: 'iife'
  },
  name: 'rrtvs',
  external: ['vue', 'vue-router'],
  globals: {
      'vue': 'Vue',
      'vue-router': 'VueRouter'
  },
  plugins: [
    typescript(),
    vue({
      compileTemplate: true,
      css: 'dist/static/css/bundle.css'
    })
  ]
};
