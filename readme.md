# Rust Rocket Typscript Vue Scaffold

If the title didn't already gave it aways, this is a scaffold project to build a web application that uses Rust/Rocket on the backend and Vue/Typescript on the frontend. Obviously, there are a few more libraries involved, but I think the repo title is long enough as it is.

## Quickstart

Install and build the project on your machine

### Frontend

* Install NODEJS & NPM ( https://nodejs.org/en/ )
* Install rollup ( https://github.com/rollup/rollup )
* Install typescript ( https://www.typescriptlang.org/ )
* Install the JS dependencies ( `npm install` )
* Download the static dependencies ( **TODO google drive link** )

### Backend

 * Install rust WITH **rustup** and **cargo** ( https://www.rust-lang.org/en-US/ )
 * Follow the requirements to run Rocket ( https://rocket.rs/guide/getting-started/ )

### Database

**Windows Alert**! On Windows, getting diesel-rs to work (the ORM used in this project) is a huge pain in the ass. If running linux is not an option,
follow *Setup diesel on Windows* in the **Appendix** section at the end of this document.

 * Install PostgreSQL
 * Install the diesel CLI  `cargo install diesel_cli --no-default-features --features postgres`
 * Rename `template.env` to `.env` 
 * Edit the `.env` file to match your environment
 * Create the database ( `diesel setup` )
 * Run the migrations ( `diesel migration run` )

## Run Instructions

  * Compile/Run the server ( `cargo run` )
  * Run rollup ( `rollup -c -w` )
  * Go to `localhost:8000`


## Appendix

### Setup diesel on Windows

This guide assumes the 64bits MSVC build of rust is used

**Fix the linking**

The first error happens at the linking phase after compiling `diesel` or `diesel-cli`: `fatal error LNK1181: cannot open input file 'libpq.lib'`
* Install the postgresql library. The "easiest" way I found is to use [vcpkg](https://github.com/Microsoft/vcpkg) 
* With vcpkg installed, run `vcpkg install libpq:x64-windows`
* Copy `libpq.lib` from `[vcpkgdir]\packages\libpq_x64-windows\lib` to your rust installation directory (ex: `.multirust\toolchains\nightly-x86_64-pc-windows-msvc\lib\rustlib\x86_64-pc-windows-msvc\lib`)
* Install the diesel CLI

**Fix the CLI**

The second error happens when running the diesel CLI OR when diesel codegen is buit. There are missing runtime dependencies (os error 126).
* Create a `windows_diesel_cli_runtime` folder in the project root
* If the files in the next step can't be found, install the windows SDK ( https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk )
* Copy the following dlls from the Windows Kit dll directory to the new folder (usually located under `C:\Program Files (x86)\Windows Kits\10\Redist\ucrt\DLLs\x64`)
  * `api-ms-win-crt-heap-l1-1-0.dll`
  * `api-ms-win-crt-locale-l1-1-0.dll`
  * `api-ms-win-crt-math-l1-1-0.dll`
  * `api-ms-win-crt-runtime-l1-1-0.dll`
  * `api-ms-win-crt-stdio-l1-1-0.dll`
  * `api-ms-win-crt-string-l1-1-0.dll`
* Copy `libpq.dll` from `[vcpkgdir]\packages\libpq_x64-windows\bin` to the new folder
* Copy the `.env` file  into the new folder and rename it to `env.sh`
* Add the new folder to your path (`PATH=$PATH:./windows_diesel_cli_runtime/`)
* From the project directory run `./windows_diesel_cli_runtime/env.sh` (this assumes MINGW is used)
* Use the cli from this console / build the server

