table! {
    cats (id) {
        id -> Int4,
        name -> Varchar,
        sex -> Bpchar,
        age -> Int2,
        coat -> Int2,
        race -> Int2,
        declawed -> Bool,
        adopted -> Bool,
        description -> Nullable<Text>,
    }
}
