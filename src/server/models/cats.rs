use super::super::schema::cats;

#[derive(Serialize, Queryable)]
pub struct Cat {
    pub id: i32,
    pub name: String,
    pub sex: String,
    pub age: i16,         // Note: age is in month
    pub coat: i16,
    pub race: i16,
    pub declawed: bool,
    pub adopted: bool,
    pub description: Option<String>
}

#[derive(Insertable, FromForm)]
#[table_name="cats"]
pub struct InsertCat {
    pub name: String,
    pub sex: String,
    pub age: i16,
    pub coat: i16,
    pub race: i16,
    pub declawed: bool,
    pub adopted: bool,
    pub description: Option<String>
}

impl Default for InsertCat {
    fn default() -> InsertCat {
        InsertCat {
            name: "Mittens".to_string(),
            sex: "f".to_string(),
            age: 12,
            coat: 0,
            race: 0,
            declawed: false,
            adopted: false,
            description: None
        }
    }
}
