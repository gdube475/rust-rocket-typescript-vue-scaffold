/*!
 * Seed the database
 * 
 * call `server seed` to run the function
 */

use diesel;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use rand::os::OsRng;
use rand::Rng;

use super::super::models::{Cat, InsertCat};
use super::super::schema::cats;

const MIN_AGE: i16 = 1;
const MAX_AGE: i16 = 12*15;

/// The seed command do not execute anything in release mode
#[cfg(not(debug_assertions))]
pub fn seed(conn: PgConnection) {}

#[cfg(debug_assertions)]
pub fn seed(conn: &PgConnection) {
  let mut rng = OsRng::new().unwrap();

  for _ in 0..30 {
    let new_cat = InsertCat {
      name: seed_cat_name(&mut rng),
      sex: random_sex(&mut rng),
      age: rng.gen_range(MIN_AGE, MAX_AGE),
      coat: 0,
      race: 0,
      declawed: false,
      adopted: false,
      description: None
    };

    diesel::insert_into(cats::table)
        .values(&new_cat)
        .get_result::<Cat>(conn)
        .expect("Error saving new cat");
  }

  println!("Database seeded!")
}

fn random_sex(rng: &mut OsRng) -> String {
  match rng.gen_weighted_bool(2) {
    true => "f",
    false => "m",
  }.to_string()
}

fn seed_cat_name(rng: &mut OsRng) -> String {
  let prefixes = ["Mr.", "Mrs.", "Doctor", "General", "Sir", "Princess", "Overlord", "King", "Master"];
  let names = ["Pancake", "Muffins", "Tao", "Daenerys", "John", "Biscuit", "Poof", "Sky", "Charlie", "Penny", "Sugar", "Honey", "Ethan", "Toasty", "Pickle"];
  let suffixes = ["Wise", "Great", "Fabulous", "Fluffball", "Queen", "Hunter", "Immortal", "Mother of the Dragons", "Floofy", "Champion", "Gentle", "Lion"];

  let (prefix, name, suffix) = (
    rng.choose(&prefixes).unwrap(),
    rng.choose(&names).unwrap(),
    rng.choose(&suffixes).unwrap(),
  );

  format!("{} {} the {}", prefix, name, suffix)
}
