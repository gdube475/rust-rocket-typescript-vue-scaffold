/*!
 * Tasks that can be run by calling the server binary with arguments
 */

mod seed;

pub use self::seed::seed;
