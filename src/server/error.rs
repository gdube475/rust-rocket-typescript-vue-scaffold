/*!
 * 
 */
use std::io::Cursor;

use rocket::request::Request;
use rocket::response::{self, Response, Responder};
use rocket::http::{ContentType, Status};

#[derive(Debug)]
pub struct AppError {
    msg: String,
    status: Status
}

impl AppError {
    pub fn not_implemented() -> AppError {
        AppError{ msg: "Not implemented".to_string(), status: Status::NotImplemented }
    }
}

impl<'r> Responder<'r> for AppError {

    fn respond_to(self, _: &Request) -> response::Result<'r> {
        Response::build()
            .sized_body(Cursor::new(self.msg))
            .status(self.status)
            .header(ContentType::Plain)
            .ok()
    }

}
