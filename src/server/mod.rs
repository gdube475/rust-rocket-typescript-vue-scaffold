mod error;
mod db;
mod controllers;
mod tasks;

pub mod schema;
pub mod models;

use rocket;
pub use self::db::DbConn;
pub use self::error::AppError;

/**
 * Run a command and exit the app if arguments were specified
 */
pub fn command() {
  use std::env;
  use std::process::exit;
  use self::tasks::*;

  let args = env::args();
  let conn = db::new_connection();

  if args.len() > 1 {
    let arg = args.skip(1).take(1).next().expect("What?");
    match arg.as_str() {
      "seed" => seed(&conn),
      other => panic!("Command {:?} is not defined", other)
    }

    exit(0)
  }
}

/**
 * Run the server
 */
pub fn run() {
    rocket::ignite()
      .manage(db::init_pool())
      .mount("/", controllers::routes())
      .launch();
}
