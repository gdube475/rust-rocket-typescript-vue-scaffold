mod app_controller;
mod cats_controller;

use rocket::Route;

pub fn routes() -> Vec<Route> {
    let mut routes: Vec<Route> = Vec::with_capacity(64);
    routes.append(&mut app_controller::routes());
    routes.append(&mut cats_controller::routes());
    routes
}