/*! 
 * Controller that handles the main application routes
 */

use rocket::Route;

use std::path::{Path, PathBuf};
use rocket::response::NamedFile;

#[cfg(not(debug_assertions))] const STATIC_PATH: &'static str = "./static/";
#[cfg(debug_assertions)] const STATIC_PATH: &'static str = "./dist/static/";

#[get("/")]
fn index() -> Option<NamedFile> {
    let path = Path::new(STATIC_PATH).join("html/index.html");
    NamedFile::open(path).ok()
}

#[get("/static/<file..>")]
fn static_files(file: PathBuf) -> Option<NamedFile> {
    let path = Path::new(STATIC_PATH).join(file);
    NamedFile::open(path).ok()
}

pub fn routes() -> Vec<Route> {
    routes![index, static_files]
}
