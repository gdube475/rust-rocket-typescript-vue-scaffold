/**
 * CRUD routes for the cats
 */

use rocket::Route;
use rocket::request::Form;
use rocket_contrib::Json;
use diesel::pg::PgConnection;
use diesel::prelude::*;
use diesel;
use super::super::models::{Cat, InsertCat};
use super::super::{DbConn, AppError};


#[get("/cats", format = "application/json")]
fn index(conn: DbConn) -> Json<Vec<Cat>>  {
  use super::super::schema::cats::dsl::*;
  let cat_list = cats.load::<Cat>(&*conn);
  if cat_list.is_ok() {
    Json(cat_list.unwrap())
  } else {
    Json(Vec::new())
  }
}

#[post("/cats/create", data="<cat_data>")]
fn create(conn: DbConn, cat_data: Form<InsertCat>) -> Result<String, AppError>  {
  use super::super::schema::cats;
  let new_cat: InsertCat = cat_data.into_inner();

  let pg_conn: &PgConnection = &conn; 
  match diesel::insert_into(cats::table).values(&new_cat).get_result::<Cat>(pg_conn) {
    Ok(_) => Ok("Cat created".to_string()),
    Err(_) => Err(AppError::not_implemented())
  }
}

pub fn routes() -> Vec<Route> {
    routes![index, create]
}