/*!
 * Passthrough main. 
*/

#![recursion_limit="128"]
#![feature(plugin)]
#![feature(custom_derive)]
#![plugin(rocket_codegen)]

extern crate rocket;
extern crate rocket_contrib;

#[macro_use] extern crate diesel;

extern crate r2d2_diesel;
extern crate r2d2;

extern crate dotenv;
extern crate rand;

extern crate serde;
extern crate serde_json;
#[macro_use] extern crate serde_derive;

use dotenv::dotenv;

mod server;

fn main() {
    dotenv().ok();
    server::command();
    server::run();
}
