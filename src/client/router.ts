import VueRouter from 'vue-router';
import { Home, CatsIndex, CatsForm, UsersIndex, SuppliesIndex } from './views/_';

function homeRoutes() {
  console.log(main_routes);
  return {routes: main_routes.filter((r) => r.name != 'Homepage')};
}

// Base route for the controllers. Also displayed in the sidenav and in the homepage
export const main_routes = [
  { path: '/', component: Home, props: homeRoutes, name: 'Homepage', icon: 'i-home'},
  { path: '/cats', component: CatsIndex, name: 'Cats', icon: 'i-cat'},
  { path: '/users', component: UsersIndex, name: 'Users', icon: 'i-user'},
  { path: '/supplies', component: SuppliesIndex, name: 'Supplies', icon: 'i-truck'}
];

export const sub_routes = [
  { path: '/cats/new', component: CatsForm }
];

export const routes = main_routes.concat(<any[]>sub_routes);

export const Router = new VueRouter({
  routes
});
