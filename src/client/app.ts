import Vue from 'vue';
import { Router, main_routes } from "./router";
import { App } from './views/_';

new Vue({
  el: '#app',
  render: h => h(App, {
    props: {routes: main_routes}
  }),
  router: Router
})
