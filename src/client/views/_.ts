import { CatsIndex, CatsForm } from "./cats/_";
import { UsersIndex } from "./users/_";
import { SuppliesIndex } from "./supplies/_";
import Home from "./Home.vue";
import App from "./App.vue";

export { CatsIndex, CatsForm, UsersIndex, SuppliesIndex, Home, App }
