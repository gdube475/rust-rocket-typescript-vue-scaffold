// Allow the importation of .vue file from the editor.
declare module "*.vue" {
    import Vue from 'vue';
    export default Vue;
}
