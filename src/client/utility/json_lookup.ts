function keyLookup(obj: any, pathKeys: Array<string>): any {
  const nextPathKey = pathKeys.splice(0, 1)[0];
  if (obj.hasOwnProperty(nextPathKey)) {
    if (pathKeys.length == 0) {
      return obj[nextPathKey];
    } else {
      return keyLookup(obj[nextPathKey], pathKeys);
    }
  }

  return undefined;
}

export function jsonLookup(obj: any, path: string): any {
  const pathKeys = path.split('.');
  return keyLookup(obj, pathKeys);
}
